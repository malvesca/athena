/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/Decorator.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Oct, 2023
 * @brief Helper class to provide type-safe access to aux data.
 */


#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/exceptions.h"


namespace SG {


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 *
 * The name -> auxid lookup is done here.
 */
template <class T, class ALLOC>
inline
Decorator<T, ALLOC>::Decorator (const std::string& name)
  : Decorator (name, "", SG::AuxVarFlags::None)
{
  // cppcheck-suppress missingReturn
}


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 * @param clsname The name of its associated class.  May be blank.
 *
 * The name -> auxid lookup is done here.
 */
template <class T, class ALLOC>
inline
Decorator<T, ALLOC>::Decorator (const std::string& name,
                                const std::string& clsname)
  : Decorator (name, clsname, SG::AuxVarFlags::None)
{
}


/**
 * @brief Constructor taking an auxid directly.
 * @param auxid ID for this auxiliary variable.
 *
 * Will throw @c SG::ExcAuxTypeMismatch if the types don't match.
 */
template <class T, class ALLOC>
inline
Decorator<T, ALLOC>::Decorator (const SG::auxid_t auxid)
  : Decorator (auxid, SG::AuxVarFlags::None)
{
  // cppcheck-suppress missingReturn; false positive
}


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 * @param clsname The name of its associated class.  May be blank.
 * @param flags Optional flags qualifying the type.  See AuxTypeRegsitry.
 *
 * The name -> auxid lookup is done here.
 */
template <class T, class ALLOC>
inline
Decorator<T, ALLOC>::Decorator
  (const std::string& name,
   const std::string& clsname,
   const SG::AuxVarFlags flags)
  : m_auxid (SG::AuxTypeRegistry::instance().getAuxID<T, ALLOC> (name, clsname, flags))
{
}


/**
 * @brief Constructor taking an auxid directly.
 * @param auxid ID for this auxiliary variable.
 * @param flags Optional flags qualifying the type.  See AuxTypeRegistry.
 *
 * Will throw @c SG::ExcAuxTypeMismatch if the types don't match.
 */
template <class T, class ALLOC>
inline
Decorator<T, ALLOC>::Decorator (const SG::auxid_t auxid,
                                const SG::AuxVarFlags flags)
  : m_auxid (auxid)
{
  SG::AuxTypeRegistry::instance().checkAuxID<T, ALLOC> (auxid, flags);
}


/**
 * @brief Fetch the variable for one element, as a non-const reference.
 * @param e The element for which to fetch the variable.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsConstAuxElement<ELT> )
inline
typename Decorator<T, ALLOC>::reference_type
Decorator<T, ALLOC>::operator() (const ELT& e) const
{
  assert (e.container() != 0);
  return e.container()->template getDecoration<T> (m_auxid, e.index());
}

/**
 * @brief Fetch the variable for one element, as a non-const reference.
 * @param container The container from which to fetch the variable.
 * @param index The index of the desired element.
 *
 * This allows retrieving aux data by container / index.
 * Looping over the index via this method will be faster then
 * looping over the elements of the container.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
inline
typename Decorator<T, ALLOC>::reference_type
Decorator<T, ALLOC>::operator() (const AuxVectorData& container,
                                                  size_t index) const
{
  return container.template getDecoration<T> (m_auxid, index);
}


/**
 * @brief Set the variable for one element.
 * @param e The element for which to fetch the variable.
 * @param x The variable value to set.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsConstAuxElement<ELT> )
inline
void Decorator<T, ALLOC>::set (const ELT& e,
                               const element_type& x) const
{
  (*this)(e) = x;
}


/**
 * @brief Get a pointer to the start of the auxiliary data array.
 * @param container The container from which to fetch the variable.
 */
template <class T, class ALLOC>
inline
typename Decorator<T, ALLOC>::const_container_pointer_type
Decorator<T, ALLOC>::getDataArray (const AuxVectorData& container) const
{
  return reinterpret_cast<const_container_pointer_type>
    (container.getDataArray (m_auxid));
}


/**
 * @brief Get a pointer to the start of the auxiliary data array.
 * @param container The container from which to fetch the variable.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
inline
typename Decorator<T, ALLOC>::container_pointer_type
Decorator<T, ALLOC>::getDecorationArray (const AuxVectorData& container) const
{
  return reinterpret_cast<container_pointer_type>
    (container.getDecorationArray (m_auxid));
}


/**
 * @brief Get a span over the auxilary data array.
 * @param container The container from which to fetch the variable.
 */
template <class T, class ALLOC>
inline
typename Decorator<T, ALLOC>::const_span
Decorator<T, ALLOC>::getDataSpan (const AuxVectorData& container) const
{
  auto beg = reinterpret_cast<const_container_pointer_type>
    (container.getDataArray (m_auxid));
  return const_span (beg, container.size_v());
}


/**
 * @brief Get a span over the auxilary data array.
 * @param container The container from which to fetch the variable.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
inline
typename Decorator<T, ALLOC>::span
Decorator<T, ALLOC>::getDecorationSpan (const AuxVectorData& container) const
{
  auto beg = reinterpret_cast<container_pointer_type>
    (container.getDecorationArray (m_auxid));
  return span (beg, container.size_v());
}


/**
 * @brief Test to see if this variable exists in the store.
 * @param e An element of the container in which to test the variable.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsConstAuxElement<ELT> )
inline
bool
Decorator<T, ALLOC>::isAvailable (const ELT& e) const
{
  return e.container() && e.container()->isAvailable (m_auxid);
}

/**
 * @brief Test to see if this variable exists in the store and is writable.
 * @param e An element of the container in which to test the variable.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsConstAuxElement<ELT> )
inline
bool
Decorator<T, ALLOC>::isAvailableWritable (const ELT& e) const
{
  return e.container() && e.container()->isAvailableWritableAsDecoration (m_auxid);
}


/**
 * @brief Test to see if this variable exists in the store.
 * @param c The container in which to test the variable.
 */
template <class T, class ALLOC>
inline
bool
Decorator<T, ALLOC>::isAvailable (const AuxVectorData& c) const
{
  return c.isAvailable (m_auxid);
}

/**
 * @brief Test to see if this variable exists in the store and is writable.
 * @param c The container in which to test the variable.
 */
template <class T, class ALLOC>
inline
bool
Decorator<T, ALLOC>::isAvailableWritable (const AuxVectorData& c) const
{
  return c.isAvailableWritableAsDecoration (m_auxid);
}


/**
 * @brief Return the aux id for this variable.
 */
template <class T, class ALLOC>
inline
SG::auxid_t
Decorator<T, ALLOC>::auxid() const
{
  return m_auxid;
}


} // namespace SG
